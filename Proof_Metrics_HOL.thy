(*
 * Copyright 2014, NICTA
 *
 * This software may be distributed and modified according to the terms of
 * the BSD 2-Clause license. Note that NO WARRANTY is provided.
 * See "LICENSE_BSD2.txt" for details.
 *
 * @TAG(NICTA_BSD)
 *)

theory Proof_Metrics_HOL
imports ProofGraph_HOL
begin

ML_file "proof_metrics.ML"
                
ML {* 
  structure Proof_Metrics = Proof_Metrics (
    structure Proof_Graph = Proof_Graph_HOL 
    structure Metric_Out = 
      struct 
        @{xml_record per_lemma_metric =
          "
           thy_refs : (string * int) list,
           meson : Proof_Metric_HOL.T"
        }
          let open XML.Encode in
            (list (pair string int),
             Proof_Metric_HOL.encode)
          end
          let open XML.Decode in
            (list (pair string int),
             Proof_Metric_HOL.decode)
          end
      
        val encode_per_lemma = per_lemma_metric_encode;
        val decode_per_lemma = per_lemma_metric_decode;
      
        fun output
          (full_spec : Proof_Graph.full_spec)
          (full_name,(proof_entry : Proof_Graph.proof_entry)) =
          let
            val prems = #prems proof_entry
            val concl = #concl proof_entry
            val data = #data proof_entry
            val lemma_thy_refs = String_Graph.immediate_succs (#proof_graph full_spec) full_name
              |> distinct (op =)
              |> map (Long_Name.explode #> hd)
              
            val const_thy_refs = (flat prems) @ concl
              |> distinct (op =)
              |> map (fn i => Int_Graph.get_node (#const_graph full_spec) i)
              |> map (fn {name,...} => Long_Name.explode name |> hd)
              
            val thy_refs = lemma_thy_refs @ const_thy_refs
              |> map (fn thy_ref => (thy_ref,()))
              |> Symtab.make_list
              |> Symtab.dest
              |> map (apsnd length)
            
              
          in
            case data of NONE => error "Missing proof data"
              | SOME {kind, metric ,geometry} =>
                (* let
                  val max_depth = Bracket_List.max_depth geometry;                 
                in *)
                  {
                   thy_refs = thy_refs,
                   meson = metric} : per_lemma_metric
                (* end *)
            end

            @{xml_record global_metric =
            "external_thy_refs : ((string * string) * int) list"
             }
              let open XML.Encode in
                list (pair (pair string string) int)
              end
              let open XML.Decode in
                list (pair (pair string string) int)
              end
           
           val encode_global = global_metric_encode;
           val decode_global = global_metric_decode;
            
            fun output_global
              (full_spec : Proof_Graph.full_spec)
              (per_lemma_metrics : (string * per_lemma_metric) list) =
            let
              val external_thy_refs = map (fn (_,{thy_refs,...}) => thy_refs) per_lemma_metrics
              |> flat
              |> filter_out (fn (thy,_) => member (op =) (#source_thys full_spec) thy)
              |> Symtab.make_list
              |> Symtab.map (K Integer.sum)
              |> Symtab.dest
              |> map (apfst (fn thynm => case (Symtab.lookup (#thy_deps full_spec) thynm)
                                   of SOME (filenm,_) => (thynm,filenm)
                                    | NONE => error ("Missing theory: " ^ thynm)))
              
            in
              {external_thy_refs = external_thy_refs}
            end
              
          
      end) 
  *}
  

end
