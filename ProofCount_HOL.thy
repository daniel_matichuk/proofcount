(*
 * Copyright 2014, NICTA
 *
 * This software may be distributed and modified according to the terms of
 * the BSD 2-Clause license. Note that NO WARRANTY is provided.
 * See "LICENSE_BSD2.txt" for details.
 *
 * @TAG(NICTA_BSD)
 *)

theory ProofCount_HOL
imports Main
begin

ML_file "$ISABELLE_PROOFCOUNT_HOME/proofcount_tools.ML"
ML_file "$ISABELLE_PROOFCOUNT_HOME/proof_count.ML"
                                


ML {* 

@{xml_record meson =
     "unit_eq : bool,
      eq : bool,
      horn : bool,
      num_clauses : int,
      num_literals : int"}

datatype Meson_Result = Meson of meson | Error | TimeOut | TooMany

structure Proof_Metric_HOL =
struct
  fun classify ctxt th =
  let
    val ctxt = Config.put Meson.max_clauses 129 ctxt
  
    fun literals_of th =
      let
        val t = Thm.prop_of th;
        val t' = perhaps (try HOLogic.dest_Trueprop) t;
        val lits = HOLogic.disjuncts t';
      in
        lits
      end;

    fun is_eq_literal lit = can HOLogic.dest_eq lit;

    fun is_unit_eq_clause [lit] = is_eq_literal lit
      | is_unit_eq_clause _ = false;

    fun is_eq_clause lits = exists is_eq_literal lits;

    fun is_positive_literal lit = not (can HOLogic.dest_not lit);

    fun is_horn_clause lits = length (filter is_positive_literal lits) <= 1;

    val too_many = Meson.has_too_many_clauses ctxt (Thm.concl_of th);
    val clauses = if too_many then [] else snd (Meson_Clausify.cnf_axiom ctxt false false 0 th);
    val too_many = too_many orelse length clauses >= Config.get ctxt Meson.max_clauses;
    val clauses = if too_many then [] else clauses;
    val literalss = map literals_of clauses;
  in
    if too_many then
      TooMany
    else
    Meson {unit_eq = length clauses = 1 andalso forall is_unit_eq_clause literalss,
     eq = length clauses = 1 andalso forall is_eq_clause literalss,
     horn = length clauses = 1 andalso forall is_horn_clause literalss,
     num_clauses = length clauses,
     num_literals = length (flat literalss)}
  end;
  
      
  type T = Meson_Result
  
  
 fun metric ctxt _ thm =
  let
    val classify = classify ctxt
    val limit = Timeout.apply(Time.fromSeconds 10) 
      (fn thm => case (try classify) thm of SOME result => result | NONE => Error)
  in
    limit thm handle Timeout.TIMEOUT _=> TimeOut
  end

  val version = "1"
    
  val encode = 
    curry (XML.Encode.pair XML.Encode.string (ProofCount_Tools.encode_variant [fn Meson meson => (["Meson"],meson_encode meson),
                        fn Error => (["Error"],XML.Encode.unit ()),
                        fn TimeOut => (["TimeOut"],XML.Encode.unit ()),
                        fn TooMany => (["TooMany"],XML.Encode.unit ())])) version;
  
  fun decode b = 
  let
    val (dversion,result) = 
  XML.Decode.pair XML.Decode.string
  (ProofCount_Tools.decode_variant 
    [fn (["Meson"],meson) => Meson (meson_decode meson),
     fn (["Error"],_) => Error,
     fn (["TimeOut"],_) => TimeOut,
     fn (["TooMany"],_) => TooMany]) b
   in
      if dversion = version then result else
        error ("Unexpected metric version: " ^ dversion)
   end
                         
                                           
end

structure ProofCount_HOL = Proof_Count(Proof_Metric_HOL);

*}

end
