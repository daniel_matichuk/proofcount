(*
 * Copyright 2014, NICTA
 *
 * This software may be distributed and modified according to the terms of
 * the BSD 2-Clause license. Note that NO WARRANTY is provided.
 * See "LICENSE_BSD2.txt" for details.
 *
 * @TAG(NICTA_BSD)
 *)

theory ProofGraph_HOL
imports ProofCount_HOL
begin

ML_file "$ISABELLE_PROOFCOUNT_HOME/spec_graph.ML"
ML_file "$ISABELLE_PROOFCOUNT_HOME/proof_graph.ML"


ML {* structure Proof_Graph_HOL = Proof_Graph(
  structure Proof_Count = ProofCount_HOL 
  structure Metric = Proof_Metric_HOL); *}
  

end
